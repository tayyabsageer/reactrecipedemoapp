import { useEffect, useState } from 'react';
import './App.css';
import Recipe from "./Recipe";

function App() {
  const APP_ID = 'c34c96aa'
  const APP_KEY = 'd0e80d56dcc21cb22a1df5da20848001'
  
  const [recipes,setRecipes] = useState([]);
  const [search,setSearch]= useState("")
  const [query,setQuery]=useState("Chicken")
  const url = "https://api.edamam.com/search?q="+query+"&app_id="+APP_ID+"&app_key="+APP_KEY

  useEffect(() => {
    getRecipes();
  },[query])

  const getSearch = (e) => {
    e.preventDefault();
    setQuery(search)
  }



  function HandleChange(e)
  {
      setSearch(e.target.value)
  }
 


  const getRecipes = async () => {
    const response = await fetch(url)
    const data = await response.json()
    console.log(data.hits);
    setRecipes(data.hits)
  }

  return (
    <div className="App">
      <form onSubmit={getSearch} className="form">
         <input className="SearchText" type="text" value={search} onChange={HandleChange}/>
         <button className="SeachButton"   type="submit">Search</button>
      </form>
      <div className="boxup">
       {recipes.map(recipe =>(
         <Recipe
           title={recipe.recipe.label}
           description={recipe.recipe.description}
           image={recipe.recipe.image}
           calories={recipe.recipe.calories}
           ingredientLines={recipe.recipe.ingredientLines}
           totalTime={recipe.recipe.totalTime}
           totalNutrients={recipe.recipe.totalNutrients}
         />
       ))}
    </div>
    </div>
  
  );
}

export default App;
