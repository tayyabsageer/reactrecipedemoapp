import React from "react"
const Recipe = ({title,description,image,calories,ingredientLines,totalTime,totalNutrients}) => {

    return (
        <div className="box">
            
            <h1>Title: {title}</h1>
            <h1>Description: {description}</h1>
            <img src={image}/>
            <h1>Calories:{calories}</h1>
            <h1>Total Time: {totalTime}</h1>
            <h1>ingredientLines</h1>
            <p>{ingredientLines}</p>
        </div>
    )
    {totalNutrients.map( item =>
        console.log(item)
    )}
}
export default Recipe